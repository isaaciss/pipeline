package br.com.tecnisys.pipeline;

public class Resultado {
    private Double soma;
    private Double n1;
    private Double n2;

    public Double getSoma() {

        return soma;
    }

    public void setSoma(Double soma) {
        this.soma = soma;
    }

    public Double getN1() {
        return n1;
    }

    public void setN1(Double n1) {
        this.n1 = n1;
    }

    public Double getN2() {
        return n2;
    }

    public void setN2(Double n2) {
        this.n2 = n2;
    }
}
